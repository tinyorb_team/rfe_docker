FROM tinyorb/python27djangorobotframework
COPY copiable_content/RFE /tmp/RFE
COPY ./copiable_content/run_config.sh /tmp
RUN InstallDirectory=/tmp/RFE && LOG_PATH=/var/log/rfe.log && touch $LOG_PATH  && cp $InstallDirectory/App1/robot_runner/track.json-orig $InstallDirectory/App1/robot_runner/track.json && cp $InstallDirectory/App1/meta.json-orig $InstallDirectory/App1/meta.json && cp $InstallDirectory/App1/all_manual.json-orig $InstallDirectory/App1/all_manual.json
RUN ["chmod", "+x", "/tmp/run_config.sh"]
WORKDIR /tmp
CMD ["/tmp/run_config.sh"]

